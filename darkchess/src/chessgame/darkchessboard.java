package chessgame;


import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.*;

//暗棋棋盤
public class darkchessboard extends chessboard{

	public darkchessboard() {
		super();
		washpiece wa = new washpiece();
		wa.cleararray();
		wa.setrand();
	 
		dkp[0]= new darkpiece("紅","相",5,0);
		dkp[1] = new darkpiece("紅","相",5,1);
		dkp[2] = new darkpiece("紅","仕",6,2);
		dkp[3] = new darkpiece("紅","仕",6,3);
		dkp[4] = new darkpiece("紅","帥",7,4);
		dkp[5] = new darkpiece("紅","俥",4,5);
		dkp[6] = new darkpiece("紅","俥",4,6);
		dkp[7] = new darkpiece("紅","傌",3,7);
		dkp[8] = new darkpiece("紅","傌",3,8);
		dkp[9] = new darkpiece("紅","炮",2,9);
		dkp[10] = new darkpiece("紅","炮",2,10);
		dkp[11] = new darkpiece("紅","兵",1,11);
		dkp[12] = new darkpiece("紅","兵",1,12);
		dkp[13] = new darkpiece("紅","兵",1,13);
		dkp[14] = new darkpiece("紅","兵",1,14);
		dkp[15] = new darkpiece("紅","兵",1,15);
		dkp[16] = new darkpiece("黑","象",5,16);
		dkp[17] = new darkpiece("黑","象",5,17);
		dkp[18] = new darkpiece("黑","士",6,18);
		dkp[19] = new darkpiece("黑","士",6,19);
		dkp[20] = new darkpiece("黑","將",7,20);
		dkp[21] = new darkpiece("黑","車",4,21);
		dkp[22] = new darkpiece("黑","車",4,22);
		dkp[23] = new darkpiece("黑","馬",3,23);
		dkp[24] = new darkpiece("黑","馬",3,24);
		dkp[25] = new darkpiece("黑","包",2,25);
		dkp[26] = new darkpiece("黑","包",2,26);
		dkp[27] = new darkpiece("黑","卒",1,27);
		dkp[28] = new darkpiece("黑","卒",1,28);
		dkp[29] = new darkpiece("黑","卒",1,29);
		dkp[30] = new darkpiece("黑","卒",1,30);
		dkp[31] = new darkpiece("黑","卒",1,31);
		for(int i=0;i<32;i++)
		{
			this.order[i]=wa.getnumber(i);
		}
		for(int i =0;i<32;i++)
		{
			point[order[i]/8][order[i]%8]=1;
			
			dkp[i].setheight(order[i]/8);
			dkp[i].setwidth(order[i]%8);
			view[order[i]/8][order[i]%8] = new darkpiece(dkp[i]);
			
		}
		
	}
	
	private darkpiece[] dkp = new darkpiece[32];
	private darkpiece view[][] = new darkpiece[4][8];
	private String now;
	int begin=0;
	int order[] = new int[32];
	int point[][] = new int[4][8];
	private int click =0;
	int donothing = 0;
	public int getclick()
	{
		return click;
	}
	private darkpiece temp;
	public darkpiece gettemp()
	{
		return temp;
		
	}
	public darkpiece getview(int i,int j)
	{
		return view[i][j];
		
	}
	public int getpoint(int i,int j)
	{
		return point[i][j];
	}
	public void show()
	{	

		if(begin!=0&&click==0)
		{
			System.out.printf("It's turn to %s now.\n",now);
		}
		else if(click == 1)
		{
			System.out.printf("Select %s \n",temp.getname());
		}
		for(int i =0;i<4;i++)
		{
			for(int j=0;j<8;j++)
			{
				//System.out.printf("%d",point[i][j]);
				if(point[i][j]==1&&view[i][j].getstate()=="off")
				{
					System.out.printf("%s","O");
				}
				else if(point[i][j]==0)
				{
					System.out.printf("%s","X");
				}
				else
				{
					System.out.printf("%s",view[i][j].getname());
				}
				
			}
			System.out.println();
		}
		System.out.println(click+" "+ donothing);
	}
	public void click(int h,int w)
	{
		
		donothing =0;
		if(view[h][w].getstate()=="off"&&click == 0)
		{
			view[h][w].flip();
			if(begin == 0)
			{
				now = view[h][w].getcolar();
				begin = 1;
			}
		}
		else
		{
			if(click == 0)
			{
				if(view[h][w].getcolar()==now&&point[h][w]==1)
				{
					temp = new darkpiece(view[h][w]);
					click = 1;
				}
				else if(view[h][w].getcolar()!=now)
				{
					click = 0;
					donothing =1;
				}
				if(point[h][w]==0)
				{
					donothing =1;
				}

			}
			else if(click == 1)
			{
				if(point[h][w] == 0)
				{
					//空格為前後左右一格
					if((temp.getwidth()==w+1||temp.getwidth()==w-1)^(temp.getheight()==h+1||temp.getheight()==h-1))
					{
						piecemove(temp,h,w);
						click = 0;
					}
					else
					{
						click = 0;
						donothing =1;
					}
				}
				else if(view[h][w].getcolar() == now)
				{
					click = 0;
					donothing =1;
				}
				//判斷包和炮的吃子
				else if((temp.getname()=="包"||temp.getname()=="炮"))
				{
					int crosspiece=0;
					if(point[h][w]==0)
					{
						click = 0;
						donothing =1;
					}
					else if(temp.getheight()-h==0||temp.getwidth()-w==0)//跟炮在同一直線或水平上
					{
						if(temp.getheight()-h==0)
						{
							
							if(temp.getwidth()>w)
							{
								for(int i=w;i<temp.getwidth();i++)
								{
									crosspiece = crosspiece +point[h][i];
								}
								if(crosspiece == 2)
								{
									pieceeat(temp,h,w);
								}
								else
								{
									donothing =1;
								}
							}
							else
							{
								for(int i=temp.getwidth();i<w;i++)
								{
									crosspiece = crosspiece +point[h][i];
								}
								if(crosspiece == 2)
								{
									pieceeat(temp,h,w);
								}
								else
								{
									donothing =1;
								}
							}
						}
						else
						{
							if(temp.getheight()>h)
							{
								for(int i=h;i<temp.getheight();i++)
								{
									crosspiece = crosspiece +point[h][i];
								}
								if(crosspiece == 2)
								{
									pieceeat(temp,h,w);
								}
								else
								{
									donothing =1;
								}
							}
							else
							{
								for(int i=temp.getheight();i<h;i++)
								{
									crosspiece = crosspiece +point[h][i];
								}
								if(crosspiece == 2)
								{
									pieceeat(temp,h,w);
								}
							}
						}
						click = 0;
					}
					else
					{
						click = 0;
						donothing =1;
	
					}
				}
				else
				{
					if(((temp.getwidth()==w+1||temp.getwidth()==w-1)^(temp.getheight()==h+1||temp.getheight()==h-1))
							&&view[h][w].getstate()=="on")
					{
						if(temp.getgrade()==1&&view[h][w].getgrade()==7)
						{
							pieceeat(temp,h,w);
							click =0;
						}
						else if(temp.getgrade()==7&&view[h][w].getgrade()==1)
						{
							click =0;
							donothing =1;
						}
						else if(view[h][w].getgrade()<=temp.getgrade())
						{
							pieceeat(temp,h,w);
							click =0;
						}
						else
						{
							click =0;
							donothing =1;
						}
					}
				}
				
			}
			
		}
		
		if(click == 0&&donothing == 0)
		{
			if(now == "黑")
			{
				now = "紅";
			}
			else
			{
				now = "黑";
			}
		}
		
	}
	public void piecemove(darkpiece temp2, int h, int w) {
		point[temp2.getheight()][temp2.getwidth()] = 0;
		temp2.setheight(h);
		temp2.setwidth(w);
		view[h][w]=new darkpiece(temp2);
		point[h][w] =1;
		view[h][w].flip();
		System.out.println("move");
		
		
	}
	public void pieceeat(darkpiece temp2, int h, int w) {
		point[temp2.getheight()][temp2.getwidth()] = 0;
		temp2.setheight(h);
		temp2.setwidth(w);
		view[h][w]=new darkpiece(temp2);
		view[h][w].flip();
		System.out.println("eat");
		
		
	}
	public String getnow() {
		// TODO Auto-generated method stub
		return now;
	}
}
