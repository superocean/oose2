package chessgame;

public abstract interface piece {
	
	public void setcolar(String color);
	public void setname(String name);
	public String getcolar();
	public String getname();
	public void setheight(int height);
	public int getheight();
	public void setwidth(int width);
	public int getwidth();
	public int getgrade();
	public void setgrade(int grade);
}
