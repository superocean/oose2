package chessgame;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JPanel;

import javax.swing.*;
import javax.swing.border.Border;

public class darkchess extends JFrame {
	
	darkchessboard dk;
	ImageIcon boardpic;
	ImageIcon piecepic;
	ImageIcon openpiecepic;
	JPanel board;
	JPanel up;
	JPanel left;
	JPanel right;
	JButton b[][] = new JButton[4][8];
	JLabel title;
	public darkchess()
	{
		clicklistener ck = new clicklistener();
		dk = new darkchessboard();
		boardpic= new ImageIcon("board.png");
		piecepic= new ImageIcon("piece.png");
		openpiecepic = new ImageIcon("openpiece.png");
		board = new JPanel();
		up = new JPanel();
		left = new JPanel();
		right = new JPanel();
		board.setLayout(new GridLayout(4,8));
		title = new JLabel("開始");
		title.setFont(new Font("TimesRoman",Font.BOLD,35));
		up.add(title);
		left.add(new JButton("aa"));
		right.add(new JButton("bb"));
		for(int i=0;i<4;i++)
		{
			for(int j=0;j<8;j++)
			{
				
				Color color = new Color(100, 60, 20);
				b[i][j]= new JButton(piecepic);
				b[i][j].setSize(100, 100);
				b[i][j].addActionListener(ck);
				b[i][j].setHorizontalTextPosition(AbstractButton.CENTER);
				b[i][j].setBackground(color);
				b[i][j].setForeground(Color.white);
				b[i][j].setFont(new Font("TimesRoman",Font.BOLD,50));
				board.add(b[i][j]);
			}
		}

		add(board,BorderLayout.CENTER);
		add(up,BorderLayout.NORTH);

		
		setLocation(200,100);
		pack();
		setVisible(true);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		

	}
	class clicklistener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			for(int i=0;i<4;i++)
			{
				for(int j=0;j<8;j++)
				{
					if(e.getSource()==b[i][j])
					{
						dk.click(i,j);
						flesh();
					}
				}
			}
			
		}
	}
	public void flesh()
	{
		if(dk.getclick()==0)
		{
			if(dk.getnow()=="紅")
			{
				title.setText("輪到  紅方");
			}
			else
			{
				title.setText("輪到  黑方");
			}
		}
		else if(dk.getclick()==1)
		{
			title.setText("選擇: "+dk.gettemp().getname());
		}
		for(int i=0;i<4;i++)
		{
			for(int j=0;j<8;j++)
			{
				if(dk.getpoint(i, j)==0)
				{
					
					b[i][j].setIcon(null);
					b[i][j].setText("");
				}
				else if(dk.getpoint(i, j)==1&&dk.getview(i, j).getstate()=="on")
				{
					b[i][j].setIcon(openpiecepic);
					if(dk.getview(i, j).getcolar()=="紅")
					{
						b[i][j].setForeground(Color.red);
					}
					else
					{
						b[i][j].setForeground(Color.BLACK);
					}
					b[i][j].setText(dk.getview(i, j).getname());
				}
			}
		}
		
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		darkchessboard dk = new darkchessboard();
		darkchess dark = new darkchess();
		dk.show();
		int i=0;
		while(i<32)
		{
			if(dk.getclick()==0)
			{
				dk.click(sc.nextInt()-1, sc.nextInt()-1);
				dk.show();
			}
			
			{
				dk.click(sc.nextInt()-1, sc.nextInt()-1);
				dk.show();
			}
		}
		
	}

}
