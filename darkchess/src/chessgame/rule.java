package chessgame;

public class rule {
	int click =0;
	int begin =0;
	String now;
	public void click(int h,int w)
	{
		
		int donothing = 0;
		if(view[h][w].getstate()=="off"&&click == 0)
		{
			view[h][w].flip();
			if(begin == 0)
			{
				now = view[h][w].getcolar();
				begin = 1;
			}
		}
		else
		{
			if(click == 0)
			{
				if(view[h][w].getcolar()==now&&point[h][w]==1)
				{
					temp = new darkpiece(view[h][w]);
					click = 1;
				}
				else if(view[h][w].getcolar()!=now)
				{
					click = 0;
					donothing =1;
				}

			}
			else if(click == 1)
			{
				if(point[h][w] == 0)
				{
					//空格為前後左右一格
					if((temp.getwidth()==w+1||temp.getwidth()==w-1)^(temp.getheight()==h+1||temp.getheight()==h-1))
					{
						piecemove(temp,h,w);
						click = 0;
					}
					else
					{
						click = 0;
						donothing =1;
					}
				}
				else if(view[h][w].getcolar() == now)
				{
					click = 0;
					donothing =1;
				}
				//判斷包和炮的吃子
				else if((temp.getname()=="包"||temp.getname()=="炮"))
				{
					int crosspiece=0;
					if(point[h][w]==0)
					{
						click = 0;
						donothing =1;
					}
					else if(temp.getheight()-h==0||temp.getwidth()-w==0)//跟炮在同一直線或水平上
					{
						if(temp.getheight()-h==0)
						{
							
							if(temp.getwidth()>w)
							{
								for(int i=w;i<temp.getwidth();i++)
								{
									crosspiece = crosspiece +point[h][i];
								}
								if(crosspiece == 2)
								{
									pieceeat(temp,h,w);
								}
								else
								{
									donothing =1;
								}
							}
							else
							{
								for(int i=temp.getwidth();i<w;i++)
								{
									crosspiece = crosspiece +point[h][i];
								}
								if(crosspiece == 2)
								{
									pieceeat(temp,h,w);
								}
								else
								{
									donothing =1;
								}
							}
						}
						else
						{
							if(temp.getheight()>h)
							{
								for(int i=h;i<temp.getheight();i++)
								{
									crosspiece = crosspiece +point[h][i];
								}
								if(crosspiece == 2)
								{
									pieceeat(temp,h,w);
								}
								else
								{
									donothing =1;
								}
							}
							else
							{
								for(int i=temp.getheight();i<h;i++)
								{
									crosspiece = crosspiece +point[h][i];
								}
								if(crosspiece == 2)
								{
									pieceeat(temp,h,w);
								}
							}
						}
						click = 0;
					}
					else
					{
						click = 0;
						donothing =1;
	
					}
				}
				else
				{
					if(((temp.getwidth()==w+1||temp.getwidth()==w-1)^(temp.getheight()==h+1||temp.getheight()==h-1))
							&&view[h][w].getstate()=="on")
					{
						if(temp.getgrade()==1&&view[h][w].getgrade()==7)
						{
							pieceeat(temp,h,w);
							click =0;
						}
						else if(temp.getgrade()==7&&view[h][w].getgrade()==1)
						{
							click =0;
							donothing =1;
						}
						else if(view[h][w].getgrade()<=temp.getgrade())
						{
							pieceeat(temp,h,w);
							click =0;
						}
						else
						{
							click =0;
							donothing =1;
						}
					}
				}
				
			}
			
		}
		
		if(click == 0&&donothing == 0)
		{
			if(now == "黑")
			{
				now = "紅";
			}
			else
			{
				now = "黑";
			}
		}
		
	}
	public void piecemove(darkpiece temp2, int h, int w) {
		point[temp2.getheight()][temp2.getwidth()] = 0;
		temp2.setheight(h);
		temp2.setwidth(w);
		view[h][w]=new darkpiece(temp2);
		point[h][w] =1;
		view[h][w].flip();
		System.out.println("move");
		
		
	}
	public void pieceeat(darkpiece temp2, int h, int w) {
		point[temp2.getheight()][temp2.getwidth()] = 0;
		temp2.setheight(h);
		temp2.setwidth(w);
		view[h][w]=new darkpiece(temp2);
		view[h][w].flip();
		System.out.println("eat");
		
		
	}

}
