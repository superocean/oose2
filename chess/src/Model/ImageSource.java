package Model;

public class ImageSource {

	  private String[] redImage = new String[]
			  {"R_General","R_Advisor","R_Advisor","R_Elephant","R_Elephant","R_Horse","R_Horse","R_Chariot","R_Chariot","R_Cannon","R_Cannon","R_Soldier","R_Soldier","R_Soldier","R_Soldier","R_Soldier"};
	  private String[] blackImage = new String[]
			  {"B_General","B_Advisor","B_Advisor","B_Elephant","B_Elephant","B_Horse","B_Horse","B_Chariot","B_Chariot","B_Cannon","B_Cannon","B_Soldier","B_Soldier","B_Soldier","B_Soldier","B_Soldier"};
	  
	  public String getRedImageSource(int index){
		  
		      return this.redImage[index];
	  }
	  public String getBlackImageSource(int index){
		  
	      return this.blackImage[index];
      }
	  
	  
}
