package Model;

public class DarkChessBoard extends ChessBoard{

	private ChessPiece piece;
	 public DarkChessBoard(int x,int y,int w,int h){
		  
		  this.startx = x;
		  this.starty = y;
		  this.width = w;
		  this.height = h;
		  System.out.println("設置長寬完畢");
		  piece = new ChessPiece(this);
		  
	  }
	  public void setStartPosition(int x,int y){
		  
		  this.startx = x;
		  this.starty = y;
		  System.out.println("設置起始位置完畢");
	  }
	  public int getStartPositionX(){
		  return this.startx;
	  }
	  public int getStartPositionY(){
		  return this.starty;
	  }
}
